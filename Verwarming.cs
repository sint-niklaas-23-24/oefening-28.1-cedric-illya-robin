﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._1
{
    
    public class Verwarming
    {
        private double _graden;

        public double Graden
        {
            get { return _graden; }
            set { _graden = value; }
        }

        public bool Power
        {
            get;
            set; // hoe dit doen zonder _power?
        }
        public double InFahrenheit()
        {
            return Graden * 1.8 + 32;
        }
    }
}
