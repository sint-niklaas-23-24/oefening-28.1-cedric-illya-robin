﻿namespace Oefening_28._1
{
    public class PLC
    {
        public PLC()
        {
            //Verwarming Chauffage = new Verwarming();
            //Lichten Salonlichten = new Lichten();
        }

        public Verwarming Chauffage
        { get; set; } //onzeker
        public Lichten Salonlichten
        { get; set; } //onzeker

        public void DoeLichtenAan()
        {
            Salonlichten.Power = true;
        }
        public void DoeLichtenUit()
        {
            Salonlichten.Power = false;
        }
        public void ZetVerwarmingAf()
        {
            Chauffage.Power = false;
        }
        public void ZetVerwarmingOp()
        {
            Chauffage.Power = true;
        }
        public void PasTemperatuurAan(double graden)
        {
            Chauffage.Graden = graden;
        }
    }
}
