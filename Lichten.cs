﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._1
{
    public class Lichten
    {
        public Lichten() 
        {
            Power = false;
        }
        public bool Power
        {
            get;
            set;
        }
    }
}
