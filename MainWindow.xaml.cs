﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_28._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        PLC plc;

        private void btnLichtSchakelaar_Checked(object sender, RoutedEventArgs e)
        {
            imgLicht.Source = new BitmapImage(new Uri("/Images/LampAan.jpg", UriKind.Relative));
            plc.DoeLichtenAan();
        }
        private void btnLichtSchakelaar_Unchecked(object sender, RoutedEventArgs e)
        {
            imgLicht.Source = new BitmapImage(new Uri("/Images/LampUit.jpg", UriKind.Relative));
            plc.DoeLichtenUit();
        }
        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            plc.PasTemperatuurAan(sldTemeratuur.Value);
            txtTemperaturen.Text = plc.Chauffage.Graden.ToString("0.00") + "°C" + Environment.NewLine + plc.Chauffage.InFahrenheit().ToString("0.00") + "°F";
        }
        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {
            if (plc.Chauffage.Power == true)
            {
                sldTemeratuur.Value++;
            }
            else
            {
                MessageBox.Show("De verwarming staat nog niet aan!", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            if (plc.Chauffage.Power == true)
            {
                sldTemeratuur.Value--;
            }
            else
            {
                MessageBox.Show("De verwarming staat nog niet aan!", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnVerwarmingSchakelaar_Checked(object sender, RoutedEventArgs e)
        {
            plc.ZetVerwarmingOp();
            sldTemeratuur.IsEnabled = true;
        }
        private void btnVerwarmingSchakelaar_Unchecked(object sender, RoutedEventArgs e)
        {
            plc.ZetVerwarmingAf();
            sldTemeratuur.IsEnabled = false;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            plc = new PLC();
            plc.Salonlichten = new Lichten();
            plc.Chauffage = new Verwarming();
            imgLicht.Source = new BitmapImage(new Uri("/Images/LampUit.jpg", UriKind.Relative));
            sldTemeratuur.IsEnabled = false;
        }
        private void imgLicht_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (plc.Salonlichten.Power == false)
            {
                btnLichtSchakelaar.IsChecked = true;
            }
            else
            {
                btnLichtSchakelaar.IsChecked = false;
            }
        }
    }
}
